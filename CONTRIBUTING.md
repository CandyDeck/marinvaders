## Contributing to marinvaders

Thank you for taking the time to contribute!

## Suggesting a feature

If you've got a good idea for a feature, then please let us know!

When suggesting a feature, make sure to:

* Check the code on GitLab to make sure it's not already hiding in an unreleased version ;)
* Considered if it's necessary in the library, or is an advanced technique that could be separately explained in an example
* Check existing issues, open and closed, to make sure it hasn't already been suggested

## Filing a bug report

If you're having trouble with our code then please raise an Issue to let us know. Be as detailed as possible, and be ready to answer questions when we get back to you.

## Submitting a pull request

If you've decided to fix a bug - even something as small as a single-letter typo - great! Anything that improves the code/documentation for all future users is warmly welcomed.

If you decide to work on a  requested feature it's best to let us (and everyone else) know what you're working on to avoid any duplication of effort. You can do this by replying to the original Issue for the request.

When contributing a new example or making a change to a library please keep your code style consistent with ours. We try to stick to the [pep8 guidelines for Python](https://www.python.org/dev/peps/pep-0008/).

#### Do

* Do use pep8 style guidelines
* Do comment your code where necessary
* Do submit only a single example/feature per pull-request
* Do include a description of what your example is expected to do
* Do add details of your example to examples/README.md if it exists

#### Don't

* Don't include any license information in your examples or contributions - our repositories are GNU GPLv3 licensed
* Don't try to do too much at once - submit one examples/bug-fix at a time, and be receptive to feedback
* Don't submit multiple variations of the same example, demonstrate one thing concisely

### If you're submitting an example

Try to do one thing, and do it concisely. Keep it simple. Don't mix too many ideas.

The ideal example should:

* demonstrate one idea, technique or API as concisely as possible in a single Python script
* *just work* when you run it. Although sometimes configuration is necessary
* be well commented and attempt to teach the user how and why it works
* document any required configuration, and how to install dependencies, etc


### Licensing

When you submit code to our libraries, you implicitly and irrevocably agree to adopt the associated licenses. You should be able to find this in the file named LICENSE.


### Submitting your code

Once you're ready to share your contribution with us you should submit it as a Pull Request.

* Be ready to receive and embrace constructive feedback.
* Be prepared for rejection; we can't always accept contributions. If you're unsure, ask first!

## Thank you!

If you have any questions, concerns or comments about these guidelines, please get in touch. 
You can do this by raising an issue.

Above all else, we hope you enjoy yourself, learn things and make and share great contributions.

Happy hacking!

-- The [IEDL](https://iedl.no) Crew
