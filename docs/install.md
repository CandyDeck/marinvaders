# Installation

There are several ways to get the MarINvaders toolkit running. 

## Running in the cloud through Binder

If you just want to try it, the easiest possibility is to start the tutorial in the [Binder](https://mybinder.org/) cloud:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/marinvaders%2Fmarinvaders/master?filepath=%2Fdocs%2Fmarinvaders.ipynb)


## Local installation 

MarINvaders is registered at PyPI and at conda-forge for installation within a conda environment.
To install use

    `pip install MariINvaders --upgrade`
    
    
and when using conda:

    `conda update -c conda-forge MarINvaders`


We recommend to use [(ana)conda](https://www.anaconda.com/products/individual) [environments](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) or [Python virtual environments](https://docs.python.org/3/tutorial/venv.html) for that:


Here are steps to create environments for both options.

- (Ana)conda (for conda 4.6 and later)

    In the terminal create a new conda environment:

    `conda create env --name marinvaders -c conda-forge marinvaders`

    This will create new environment marinvaders with marinvaders already installed.
    
    To use the environment do
    `conda activate marinvaders`



- Virtual Environment (minimum Python version: 3.7)

    In the terminal create a new virtual environment inside a directory you want to use the toolkit:

    `python3 -m venv env`

    This will create new directory **venv** 

    Activate the virtual environment:

    `source env/bin/activate`

    and install the toolkit:

    `pip install MarINvaders --upgrade`

    
## Development installation

For development, clone the [repository](https://gitlab.com/marinvaders/marinvaders). 
In the repository you will find a file * environment_dev.yml * which contains the specs for the development environment.
You can set this up locally by

    `conda create env -f environment_dev.yml`
    
The name of the resulting environment is * marinvaders_dev *, activate it with

    `conda activate marinvaders_dev`

