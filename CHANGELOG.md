# CHANGELOG


## 0.2.3 (dev)


### Docs

- finalized docs 
- added CI for doc generation - available at https://marinvaders.gitlab.io/marinvaders/
- moved tutorial notebook into the doc folder

### Tests

- added coveralls integration
- increased test coverage

## 0.2.2 (20210319)

Minor fixes to get it into conda-forge.

## 0.2.1 (20210319)

Aborted version due to PyPI failed upload

## 0.2.0 (20210319)

### API breaking changes

- hide "reported_as_aliens_and_natives" method
  
### Docs

- added mkdocs explaining the data background
- cleaned readme.md

## 0.1.0 (20210308)

### API breaking changes

- renamed reported_as_aliens_and_natives to reported_as_alien_and_native 
- renamed reported_as_aliens to reported_as_alien 
- renamed obis_elsewhere to all_occurrences 
- Added marinevaders.marinelife imports in top-level init 

### Extended fuctionality

- Species class accepts urn string 

### Development

- added CHANGELOG
- integration test with all eco-regions 
- reformated tests 
- refactored obis_elsewhere to avoid deprecation warning 

### Documentation

- reformated readme 
- refactored tutorial notebook 

## v0.0.12 (20210223)

### Bugfixes

- Typos
- Removing duplicates in in affected by invasive 

### Data

- Renamed natcon to molnar

### Development

- General refactoring

## v0.0.11 (20210211)

### Data

- New taxonomy file
- 
### Development

- Removing obsolete scripts
- 
### Bugfixes

- Typos

## Earlier versions

- see git history
