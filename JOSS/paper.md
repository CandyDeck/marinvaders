---
title: 'The MarINvaders Toolkit'
tags:
    - invasive species
    - marine biodiversity
    - ecosystems
    - aliens
    - Python
authors:
 - name: Radek Lonka
   orcid: 0000-0002-5815-8682 
   affiliation: 1
 - name: Francesca Verones
   orcid: 0000-0002-2908-328X
   affiliation: 1
 - name: Konstantin Stadler
   orcid: 0000-0002-1548-201X
   affiliation: 1
affiliations:
 - name: Industrial Ecology Programme, NTNU Trondheim, Norway.
   index: 1
date: 29 March 2021
bibliography: paper.bib

---

# Summary

The introduction and establishment of alien species to foreign ecosystems is a key threat for marine biodiversity [@katsanevakis2014_Impacts; @molnar2008_Assessing; @seebens2017_No].

Alien species are considered the most difficult to reverse pressure [@millenniumecosystemassessmentprogram2005_Ecosystems] and expected to increase in the near- and mid-term future [@seebens2021_Projecting]. Of particular concern are alien species which become established and out-compete local species on a large scale, thus becoming an invasive species.
The Northern Pacific sea-star (*Asterias amurensis*) was, for example, introduced to Australia and Tasmania around the 1990's and since then became a major threat to endangered species in the Sea around Australia, as well as disrupting Australian aquaculture [@2021_GISD]. 
Global research efforts to estimate the native distribution and alien introduction of marine species are spread over several databases. 
Principally, in combination these databases can be used to assess the native/alien status of a certain species or all species present in a marine ecoregion [@spalding2007_Marine] although they provide information with varying levels of resolution. 

# Statement of need

The largest databases for gathering information on marine species distributions are:

- The Ocean Biodiversity Information System [@_OBIS], which provides data on marine taxa and species distribution. It lacks information on the native range and alien range of species and how a specific species is affected by aliens.
- The World Register of Marine Species [@WoRMS20210312] contains information on native and alien species distributions.
- The Global Invasive Species Database [@2021_GISD] is a free, online searchable source of
    information about alien and invasive species that negatively impact biodiversity.
- NatCon [@molnar2008_Assessing] contains information on over 330 marine invasive species,
    including non-native distributions by marine ecoregion, invasion pathways,
     and ecological impact and other threat scores.
- The IUCN Red List [@_IUCN] provides information on which species are listed as being threatened by invasive non-native/alien species/diseases in the IUCN Red List of Species. 


The main challenge for cross-referencing these data sources is the varying geographic scale in which alien and native species distributions are reported. 
Thus, there is a need for a tool that automatically collects species data from all of these databases and harmonizes the distribution data across the data sources.
MarINvaders aims to close this method gap by providing a high-level interface to assess the native and alien distribution of marine species on an individual level as well as ecoregion level.

# Functionality

MarINvaders consists of a Python 3 module that queries the open access databases listed above for species data (sightings, threat levels and alien/native status) and also includes copies of the databases which can not be queried online. 
When requesting information on a specific marine ecoregion, the OBIS API (v3) is used to query all species for which there is occurrence data within that ecoregion in the OBIS database. 
Each species is then searched for in the other databases to potentially identify them as alien.

The databases provide geographical distributions on different scales. The NatCon distributions are on a marine ecoregion level.  
Most of the WoRMS distributions are either IHO Sea Areas, Exclusive Economic Zones (EEZ), or an intersect of these, and have a Marine Regions Geographic Identifier (MRGID) which is easily matched to a marine ecoregion by the use of shape-files. 
GISD does not provide such MRGID’s but instead gives quantitative distributions, such as country names. 
Most of these could still be matched to existing shape-files by comparing names, and subsequently be matched to marine ecoregions. 
All the distributions that could not automatically be matched were searched for manually and matched to one or more marine ecoregions.

The result of a query through MarINvaders are various (Geo)pandas DataFrames which can readily be used for subsequent analysis. 
In addition, MarINvaders provides several summary statistics providing a overview of the alien/native species within an ecoregion as well as the global distribution of a specific region separated into native and alien ranges. 


# Outlook

The MarINvaders toolkit is part of a  larger effort within the ERC ATLANTIS project (https://atlantis-erc.eu/) which assess the impact of human activity on marine ecosystem. MarINvaders will play a central role in upcoming case-studies and in the development of a web-platform for assessing marine environmental impacts of human activity.

# Acknowledgements
This project has received funding from the European Research Council (ERC) under the European Union’s Horizon 2020 research and innovation programme (grant agreement No 850717)


# References


